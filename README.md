# LabTools
Scripts and Things for Students


## Adobe - Currently Only Works With Premiere Pro

### Opening Newer Files on Older Computers

`OpenNewerAdobeFiles.sh`


This is a script that works for Mac and Linux in the terminal. The Adobe CC files are actually g-Zipped and contain an XML file. By adjusting the Version number in the XML file, the Adobe file can be openned by a previous version again. However, the file needs to be re-g-Zipped with the proper extension to work.

#### How to use:

_TL;DR:_

```shell
cd ~/Desktop
git clone https://github.com/apankow1234/LabTools
cd LabTools

./OpenNewerAdobeFiles.sh /path/to/your/video.prproj
```



_FULL:_

1. Open the Terminal
2. Change directories to the one you want; example: Desktop

   Type into the terminal:
```shell
cd ~/Desktop
```
3. Get Script

   Type into the terminal:
```shell
git clone https://github.com/apankow1234/LabTools
```

4. Change directories again but, this time, into the LabTools

   Type into the terminal:
```shell
cd LabTools
```

5. Type `./OpenNewerAdobeFiles.sh` file into the terminal

   Type into the terminal:
```shell
./OpenNewerAdobeFiles.sh
```

6. Drag the Adobe file in next or type out its path

   It will look something like this:
```shell
yourUserName$  ./OpenNewerAdobeFiles.sh /Users/labtech/Desktop/Andrew/video.prproj
```

7. Press Enter

   You should see your new file show up. If it doesn't appear, check in your home ("user") folder.