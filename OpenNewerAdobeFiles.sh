#! /bin/bash -f

# Get File to Downgrade
inFile=$1
if [ -z "$inFile" ]
	then
	echo "Please enter a filepath: ";
	read inFile;
fi

# Variable and File Setup
filename=`basename -- "$inFile"`;
extension="${filename##*.}";
fileTitle=`basename "${inFile}" ".${extension}"`;
directory=`dirname "${inFile}"`;
tempFile="__TEMP";

# Unzip
gzip -fdc $inFile -S $extension > $tempFile;

# Find Version
declare -i currentVersion=`awk -F"[ =\"]" '/[<]Project[ ].*[^\/][>]/ {print $12}' $tempFile`;
declare -i lineNumber=`grep -n '[<]Project[ ].*[^\/][>]' $tempFile | cut -f1 -d:`;

# Update Version
echo "Originally Version: ${currentVersion}"
echo "Downgraded Version: 1"
sed -i.bak "${lineNumber}s/${currentVersion}/1/" $tempFile;

# Recompile File
gzip -c $tempFile > "${directory}/${fileTitle}-DOWNGRADE.${extension}"

# Cleanup
find . -iname "${tempFile}*" -delete
